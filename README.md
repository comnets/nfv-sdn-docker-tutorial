# nfv-sdn-docker-tutorial

If you have questions, please contact us:
* [Juan Cabrera](juan.cabrera@tu-dresden.de)
* [Dr. Giang T. Nguyen](giang.nguyen@tu-dresden.de)
* [Prof. Dr. Frank H. P. Fitzek](frank.fitzek@tu-dresden.de)

# Content

## Prerequisites
* Participants of the tutorial need to bring their own laptop with VirtualBox software. The idea is to run the whole hands-on tasks inside a sandbox environment, which ensures sufficient and homogeneous software packages for all participants.
* A virtual machine (VM) with Docker installed, which can be downloaded from [here](https://cloudstore.zih.tu-dresden.de/index.php/s/eEUS6TxRddOZKyV). Credentials: (username, password) = (master, master).
* The VM is an exported file from VirtualBox (OVA 1.0 file) so that the it can be imported from different OS. If you don't have VirtualBox installed on your laptop, please download from [this page](https://www.virtualbox.org/wiki/Downloads). There are software versions for Windows, Mac and Linux operating systems.
* Slides of the tutorial:
    * [Hands-on](https://comnets.bitbucket.io/wcnc-tutorial/hands-on/)
* The slides are also available in an offline version in this repository. Download the file tutorial-slides.zip, uncompress it, and open the file slides.html with a browser.

## Agenda (~45 minutes)
1. Small introduction: From NFV/MEC to Docker (5 minutes)
1. Docker basic (5-10 minutes)
    * list commands
    * verion & info
        * docker info | version
    * execute: docker run
    * image: docker image ls
    * container: docker container ls
1. Dockerfile (15-20 minutes)
    * define Dockerfile
    * basic keywords: FROM, WORKDIR, COPY, RUN, EXPOSE, ENV, CMD
    * add mock-up application
    * build app
    * run app
    * post-deployment:
        * image related tasks: login, share, tag, publish
1. Docker compose (maybe)
    * mention that this is for service level (explain the diff.)
    * docker-compose.yml file
    * load balancing
1. Docker swarm (maybe)
1. some advanced content for self-study (5 minutes)
